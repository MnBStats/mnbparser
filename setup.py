from setuptools import setup, find_packages

setup(
    name='mnbparser',
    version='0.2.1',
    packages=find_packages(exclude=['test*']),
    license='MIT',
    test_suite="tests",
    author="Piotr Wilk"
)
