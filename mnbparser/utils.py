from enum import Enum


class AutoNumberEnum(Enum):
    def __new__(cls):
        value = len(cls.__members__) + 1
        obj = object.__new__(cls)
        obj._value_ = value
        return obj


def print_numbered_range(container, indent=0):
    result = []
    for index, item in enumerate(container, start=1):
        result.append(" "*indent)
        result.append(str(index))
        result.append(". ")
        result.append(str(item))
        result.append("\n")

    return "".join(result)
