from mnbparser.utils import AutoNumberEnum, print_numbered_range
from mnbparser.event import EventType
from mnbparser.event_range import find_rounds_positions, first_event_in
from mnbparser.round import Round, RoundStatus
from mnbparser.score import Score


class Spawn:
    """Represents single spawn fetched from the match log"""

    class Status(AutoNumberEnum):
        """Enum representing result of spawn processing"""
        ACCEPTED = ()
        INSUFFICIENT_NUMBER_OF_ROUNDS = ()
        NO_SPAWN_CANDIDATE = ()

    def __init__(self, start_time, map, attacking_faction, defending_faction,
                    attacking_team, defending_team, rounds, status):
        self.start_time = start_time
        self.map = map
        self.attacking_faction = attacking_faction
        self.defending_faction = defending_faction
        self.attacking_team = attacking_team
        self.defending_team = defending_team
        self.rounds = rounds
        self.status = status

    def __eq__(self, other):
        if not isinstance(other, Spawn):
            raise TypeError(other)
        return (self.map == other.map and
           self.attacking_faction == other.attacking_faction and
           self.defending_faction == other.defending_faction and
           self.attacking_team == other.attacking_team and
           self.defending_team == other.defending_team and
           self.rounds == other.rounds)

    def __repr__(self):
        return "<timestamp: {0}, status: {1}, no. of rounds: {2}>"\
            .format(self.start_time, self.status, len(self.rounds))

    def __str__(self):
        return "Spawn: start time: {0}, status: {1}, map: {2}, rounds:\n{3}"\
            .format(self.start_time,
                    self.status.name,
                    self.map,
                    print_numbered_range(self.rounds, indent=2))

    @property
    def score(self):
        _hs = 0
        _as = 0
        for r in self.rounds:
            if r.round_result == Round.RoundResult.HOME_TEAM_WIN:
                _hs += 1
            if r.round_result == Round.RoundResult.AWAY_TEAM_WIN:
                _as += 1

        return Score(_hs, _as)

    @classmethod
    @first_event_in(EventType.spawn_starting_events())
    def create(cls, events, map, attacking_faction, defending_faction, rules, teams):
        rounds = list(cls.__generate_rounds(events, rules, teams))
        team_a_tag, team_b_tag = Round.determine_which_team_is_a_and_b(rounds[0].spawned_players, teams.tags)
        return cls(
            start_time=rounds[0].start_time,
            map=map,
            attacking_faction=attacking_faction,
            defending_faction=defending_faction,
            attacking_team=team_a_tag,
            defending_team=team_b_tag,
            rounds=rounds,
            status=cls.__evaluate_spawn(rounds, rules.rounds_to_win))

    @staticmethod
    def __evaluate_spawn(rounds, min_no_of_rounds):
        if len(rounds) < min_no_of_rounds:
            return Spawn.Status.INSUFFICIENT_NUMBER_OF_ROUNDS

        for round in rounds:
            if round.status == RoundStatus.REJECTED:
                break
            if round.status == RoundStatus.ACCEPTED_WITH_SELECTION:
                return Spawn.Status.ACCEPTED
        return Spawn.Status.NO_SPAWN_CANDIDATE

    @staticmethod
    def __generate_rounds(events, rules, teams):
        round_positions = find_rounds_positions(events)
        num_of_draws = 0
        for round_number, pos in enumerate(round_positions):
            round_events = events[pos.start:pos.end + 1]
            round = Round.create(events=round_events, rules=rules, teams=teams)
            yield round

            if round.draw:
                num_of_draws += 1

            if (round.status == RoundStatus.ACCEPTED_WITH_SELECTION or
               round_number == rules.max_number_of_rounds + num_of_draws):
                break
