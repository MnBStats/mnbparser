import re
from mnbparser.player import Nickname


class LineTooShortError(Exception):
    """Raise when trying to parse empty line."""


MIN_WORDS_IN_MEANINGFUL_LINE = 5


class LogLine:
    def __init__(self, line):
        line = line.strip()
        words = line.split(" ")
        if len(words) < MIN_WORDS_IN_MEANINGFUL_LINE:
            raise LineTooShortError("Line has less than 5 words!")

        self.line = line
        self.words = words
        self.time = words[0]

    def __repr__(self):
        return "<LogLine ({0})>".format(str(self.__dict__))

    def __contains__(self, item):
        return item in self.line


class JoinLogLineWrapper:
    def __init__(self, log_line, tags):
        self.line = log_line
        self.player = Nickname.create(self.line.words[2], tags)
        self.game_id = log_line.words[9]

    def __repr__(self):
        return "<JoinLogLineWrapper ({0})>".format(str(self.__dict__))

    def __contains__(self, item):
        return item in self.line


class MapChangeLogLineWrapper:
    def __init__(self, log_line):
        self.line = log_line
        self.map = re.search('on (.*),', log_line.line).group(1)
        self.faction_one = re.search(', (.*) vs', log_line.line).group(1)
        self.faction_two = re.search('vs (.*) by', log_line.line).group(1)

    def __repr__(self):
        return "<MapChangeLogLineWrapper ({0})>".format(str(self.__dict__))

    def __contains__(self, item):
        return item in self.line


class PlayerSpawnLogLineWrapper:
    def __init__(self, log_line, tags):
        self.line = log_line
        self.player = Nickname.create(self.line.words[3][:-1], tags)
        self.side = log_line.words[5]
        self.wb_class = log_line.words[-1]

    def __repr__(self):
        return "<PlayerSpawnLogLineWrapper ({0})>".format(str(self.__dict__))

    def __contains__(self, item):
        return item in self.line


class KillLogLineWrapper:
    def __init__(self, log_line, tags):
        self.line = log_line
        self.player = Nickname.create(log_line.words[2], tags)
        self.victim = Nickname.create(log_line.words[4], tags)
        self.kill_type = log_line.words[3]

    def __repr__(self):
        return "<KillLogLineWrapper ({0})>".format(str(self.__dict__))

    def __contains__(self, item):
        return item in self.line


class KillInfoLogLineWrapper:
    def __init__(self, log_line, tags):
        self.line = log_line
        self.player = Nickname.create(log_line.words[2], tags)
        self.victim = Nickname.create(log_line.words[-1], tags)
        self.weapon_id = re.search('=(.*)>', log_line.line).group(1)

    def __repr__(self):
        return "<KillInfoLogLineWrapper ({0})>".format(str(self.__dict__))

    def __contains__(self, item):
        return item in self.line


class AssistsLogLineWrapper:
    def __init__(self, log_line, tags):
        self.line = log_line
        self.player = Nickname.create(log_line.words[2], tags)
        self.dmg = log_line.words[5]
        self.team_dmg = log_line.words[10]
        self.assists = log_line.words[16]

    def __repr__(self):
        return "<AssistsLogLineWeapper ({0}>".format(str(self.__dict__))

    def __contains__(self, item):
        return item in self.line


class ServerEventLogLineWrapper:
    def __init__(self, log_line):
        self.line = log_line

    def is_round_start(self):
        return "Round start." in self.line

    def is_map_start(self):
        return "Map start" in self.line

    def is_map_end(self):
        return "Map ended." in self.line

    def is_rounds_to_win_requirement_met(self):
        return self.line.words[7] == "3" or self.line.words[9] == "3"

    def is_flag_raised(self):
        '                 Round result: Team 2 win by flag higher.'
        return re.search('Round result: Team . win by flag', self.line.line)

    def is_draw(self):
        return re.search('Round result.* [dD]raw', self.line.line)

    def is_round_result(self):
        return re.search('Round result', self.line.line)

    def is_team_a_win(self):
        return re.search('Round result: Team 2.', self.line.line)

    def is_team_b_win(self):
        return re.search('Round result: Team 1.', self.line.line)

    def __repr__(self):
        return "<ServerEventLogLineWrapper ({0})>".format(str(self.__dict__))

    def __contains__(self, item):
        return item in self.line


