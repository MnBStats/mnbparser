from .event import Event, EventType
from .log_lines import JoinLogLineWrapper, \
    MapChangeLogLineWrapper, PlayerSpawnLogLineWrapper, \
    KillLogLineWrapper, KillInfoLogLineWrapper, AssistsLogLineWrapper, \
    ServerEventLogLineWrapper


class JoinHandler:
    def parse(self, line, tags):
        if "has joined the game with ID" in line:
            log = JoinLogLineWrapper(line, tags)

            return Event(
                EventType.PLAYER_JOINED, log.line.time,
                player=log.player,
                game_id=log.game_id)

        return Event(EventType.ERROR)


class MapChangeHandler:
    def parse(self, line, teams):
        if "Changed to" in line:
            log = MapChangeLogLineWrapper(line)

            return Event(
                EventType.MAP_CHANGE, log.line.time,
                map=log.map,
                faction_one=log.faction_one,
                faction_two=log.faction_two)

        return Event(EventType.ERROR)


class PlayerSpawnHandler:
    def parse(self, line, teams):
        if "Player:" in line and "Class:" in line:
            log = PlayerSpawnLogLineWrapper(line, teams.tags)

            return Event(
                EventType.PLAYER_SPAWNED, log.line.time,
                player=log.player,
                side=log.side,
                wb_class=log.wb_class)

        return Event(EventType.ERROR)


class KillHandler:
    def __init__(self):
        self.last_kill = None

    def parse(self, line, teams):
        if "<img=ico_" in line:
            log = KillLogLineWrapper(line, teams.tags)
            if log.victim in teams:
                event_type = KillHandler.__determine_type(
                    log.player,
                    log.victim,
                    teams)

                self.last_kill = Event(
                    event_type, log.line.time,
                    player=log.player,
                    victim=log.victim,
                    kill_type=log.kill_type)

                return self.last_kill

        elif "<weapon=" in line:
            log = KillInfoLogLineWrapper(line, teams.tags)
            if log.victim in teams:
                if self.last_kill:
                    self.last_kill.weapon_id = log.weapon_id

                return Event(
                    EventType.KILL_INFO, log.line.time,
                    player=log.player,
                    victim=log.victim,
                    weapon_id=log.weapon_id)

        return Event(EventType.ERROR)

    @staticmethod
    def __determine_type(killer, victim, teams):
        if killer.full == "":
            if victim in teams.home:
                return EventType.HOME_TEAM_SUICIDE
            if victim in teams.away:
                return EventType.AWAY_TEAM_SUICIDE

        if (killer in teams.home and victim in teams.home or
           killer in teams.away and victim in teams.away):
            return (EventType.HOME_TEAM_TEAMKILL if killer in teams.home else
                    EventType.AWAY_TEAM_TEAMKILL)

        if killer in teams.home:
            return EventType.HOME_TEAM_KILL
        elif killer in teams.away:
            return EventType.AWAY_TEAM_KILL

        return EventType.ERROR


class AssistsHandler:
    def parse(self, line, teams):
        if "damage" in line and "teamdamage" in line and "assist" in line:
            log = AssistsLogLineWrapper(line, teams.tags)

            if log.player in teams:
                return Event(
                    EventType.PLAYER_ASSIST, log.line.time,
                    player=log.player,
                    dmg=log.dmg,
                    team_dmg=log.team_dmg,
                    assists=log.assists)

        return Event(EventType.ERROR)


class ServerEventHandler:
    def parse(self, line, teams):
        if line.words[2] == "[EVENT]:":
            log = ServerEventLogLineWrapper(line)

            event_type = ServerEventHandler.__determine_type(log)
            return Event(event_type, log.line.time)

        return Event(EventType.ERROR)

    @staticmethod
    def __determine_type(log):
        if log.is_map_start():
            return EventType.MAP_START

        if log.is_round_start():
            if log.is_rounds_to_win_requirement_met():
                return EventType.SPAWN_SELECTED
            return EventType.ROUND_START

        if log.is_map_end():
            if log.is_rounds_to_win_requirement_met():
                return EventType.MAP_END_WITH_SELECTION
            return EventType.MAP_END

        if log.is_round_result():
            if log.is_draw():
                return EventType.DRAW

            # Logic below is not buggy. Admin mod logging is.
            if log.is_team_a_win():
                if log.is_flag_raised():
                    return EventType.TEAM_B_WIN_FLAG
                return EventType.TEAM_A_WIN

            if log.is_team_b_win():
                if log.is_flag_raised():
                    return EventType.TEAM_A_WIN_FLAG
                return EventType.TEAM_B_WIN

        return EventType.ERROR
