from functools import wraps
from mnbparser.event import EventType


class EventRangeInvalidInput(Exception):
    """Invalid input for event range to event range generators"""


class EventRange:
    """Represents Event boundaries
    (rounds / spawns starting and end positions)"""
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def __repr__(self):
        return "<start: {0}, end: {1}>".format(self.start, self.end)

    def __eq__(self, other):
        if not isinstance(other, EventRange):
            raise TypeError(other)
        return self.start == other.start \
            and self.end == other.end


def first_event_in(start_events):
    """Use to validate that range start with correct event types."""
    def decorator(function):
        @wraps(function)
        def inner(*args, **kwargs):
            if not kwargs["events"]:
                raise EventRangeInvalidInput(
                    "No events on the list. Cannot process range.")

            if kwargs["events"][0].type not in start_events:
                raise EventRangeInvalidInput(
                    "Events do not start with correct event. "
                    "Cannot process range.")

            return function(*args, **kwargs)
        return inner
    return decorator


def find_spawns_positions(events):
    return list(events_range_generator(
        events,
        start_events=EventType.spawn_starting_events(),
        end_events=EventType.spawn_ending_events()))


def find_map_info(events):
    for event in reversed(events):
        if event.type == EventType.MAP_CHANGE:
            return event


def find_rounds_positions(events):
    return list(events_range_generator(
        events,
        start_events=EventType.round_starting_events(),
        end_events=EventType.round_ending_events()))


def events_range_generator(events, start_events, end_events):
    """Find event range, for example: Rounds, Spawns."""
    start = None
    for index, event in enumerate(events):
        if start is not None and event.type in end_events:
            event_range = EventRange(start, end=index)
            start = None
            yield event_range

        if event.type in start_events:
            start = index

    yield from __add_last_range_in_file(end_events, events, start, start_events)


def __add_last_range_in_file(end_events, events, start, start_events):
    """EDGE CASE: The only time range not ends with end events is at the end of
    the processed log file."""
    if start is not None:
        last_index = len(events) - 1
        last_event_type = events[last_index].type
        if last_event_type not in end_events or \
                (last_event_type in end_events and
                 last_event_type in start_events):
            yield EventRange(start, last_index)
