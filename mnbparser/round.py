import itertools
from datetime import time, datetime, date, timedelta
from mnbparser.utils import AutoNumberEnum
from mnbparser.event import EventType
from mnbparser.event_range import first_event_in
from mnbparser.kill import Kill
from mnbparser.assist import Assist
from mnbparser.player_round_performance import SpawnedPlayer


class RoundStatus(AutoNumberEnum):
    """Enum representing result of round processing"""
    ACCEPTED = ()
    ACCEPTED_WITH_SELECTION = ()
    REJECTED = ()


class Round:
    """Represents output of processing single round in a log file"""

    class RoundResult(AutoNumberEnum):
        HOME_TEAM_WIN = ()
        AWAY_TEAM_WIN = ()
        DRAW = ()
        EVALUATION_IMPOSSIBLE = ()

    def __init__(self, events, start_time, home_kills, away_kills,
                 assists, spawned_players, flag, draw, status, round_result):
        self.events = events
        self.start_time = start_time
        self.home_team_kills = home_kills
        self.away_team_kills = away_kills
        self.assists = assists
        self.spawned_players = spawned_players
        self.flag = flag
        self.draw = draw
        self.status = status
        self.round_result = round_result

    def __eq__(self, other):
        if not isinstance(other, Round):
            raise TypeError(other)
        return self.home_team_kills == other.home_team_kills \
            and self.away_team_kills == other.away_team_kills

    def __repr__(self):
        return (
            "<Round: team_a_kills: {0}, "
            "team_b_kills: {1},"
            "flag raised: {2}, "
            "status: {3}> ").format(
                len(self.home_team_kills),
                len(self.away_team_kills),
                self.flag,
                self.status)

    def __str__(self):
        return ("Round: start_time: {0}, home_team_kills: {1}, "
                "away_team_kills: {2}, "
                "flag: {3}, status: {4}, result: {5}")\
            .format(self.start_time,
                    len(self.home_team_kills),
                    len(self.away_team_kills),
                    self.flag,
                    self.status.name,
                    self.round_result)

    def accepted(self):
        return self.status in {RoundStatus.ACCEPTED,
                               RoundStatus.ACCEPTED_WITH_SELECTION}

    def kills(self):
        return itertools.chain(self.home_team_kills, self.away_team_kills)

    @classmethod
    @first_event_in(EventType.round_starting_events())
    def create(cls, events, rules, teams):
        spawned_players = list(cls.__get_spawned_players(events, teams))
        home_team_kills = list(cls.__get_kills(events, EventType.home_team_scores(), teams))
        away_team_kills = list(cls.__get_kills(events, EventType.away_team_scores(), teams))
        assists = list(cls.__get_assists(events, teams, spawned_players))

        event_types = {event.type for event in events}
        flag = EventType.TEAM_A_WIN_FLAG in event_types or \
               EventType.TEAM_B_WIN_FLAG in event_types
        draw = EventType.DRAW in event_types

        round_result_event = next(
            filter(lambda e: e.type in EventType.round_result_events(), events),
            None)
        round_result_type = \
            round_result_event.type if round_result_event else None
        team_a_tag, team_b_tag = cls.determine_which_team_is_a_and_b(
            spawned_players, teams.tags)
        round_result = cls.determine_round_result(
            team_a_tag, team_b_tag, teams.tags, round_result_type)

        num_of_early_drops = cls.__count_early_drops(events)
        status = cls.__evaluate_round(
            home_team_kills,
            away_team_kills,
            num_of_early_drops,
            flag,
            draw,
            rules.required_number_of_kills,
            events[-1].type)

        return cls(
            events=events,
            start_time=events[0].time,
            home_kills=home_team_kills,
            away_kills=away_team_kills,
            assists=assists,
            spawned_players=spawned_players,
            flag=flag,
            draw=draw,
            status=status,
            round_result=round_result)

    @staticmethod
    def __get_spawned_players(events, teams):
        for event in (e for e in events if e.type == EventType.PLAYER_SPAWNED):
            player = teams.get_player(event.player)
            if player is not None:
                yield SpawnedPlayer(
                    player=player,
                    side=event.side,
                    wb_class=event.wb_class)

    @staticmethod
    def __get_kills(events, types, teams):
        kill_events = (e for e in events if e.type in EventType.kill_types())
        for kill_num, event in enumerate(kill_events, start=1):
            if event.type in types:
                teamkill = event.type in {
                    EventType.HOME_TEAM_TEAMKILL,
                    EventType.AWAY_TEAM_TEAMKILL}

                yield Kill(
                    kill_number=kill_num,
                    killer=teams.get_player(event.player),
                    victim=teams.get_player(event.victim),
                    kill_type=event.kill_type,
                    weapon_id=event.weapon_id,
                    teamkill=teamkill)

    @staticmethod
    def __get_assists(events, teams, spawned_players):
        assist_events = (e for e in events if e.type == EventType.PLAYER_ASSIST)
        for event in assist_events:
            player = teams.get_player(event.player)
            for sp in spawned_players:
                if player == sp.player:
                    yield Assist(
                        player=player,
                        dmg=event.dmg,
                        team_dmg=event.team_dmg,
                        number_of_assists=event.assists)

    @staticmethod
    def get_first_team_a_player(spawned_players):
        for sp in spawned_players:
            if sp.side == '0':
                return sp
        return None

    @staticmethod
    def determine_which_team_is_a_and_b(spawned_players, tags):
        team_a_player = Round.get_first_team_a_player(spawned_players)

        if team_a_player:
            if team_a_player.player.name.tag == tags.home:
                return tags.home, tags.away
            if team_a_player.player.name.tag == tags.away:
                return tags.away, tags.home
        return None, None

    @staticmethod
    def pick_winner(winner_tag, tags):
        if winner_tag == tags.home:
            return Round.RoundResult.HOME_TEAM_WIN
        if winner_tag == tags.away:
            return Round.RoundResult.AWAY_TEAM_WIN
        return Round.RoundResult.EVALUATION_IMPOSSIBLE

    @staticmethod
    def determine_round_result(team_a_tag, team_b_tag, tags, result_type):
        if result_type in EventType.team_a_wins():

            return Round.pick_winner(team_a_tag, tags)
        if result_type in EventType.team_b_wins():
            return Round.pick_winner(team_b_tag, tags)
        if result_type == EventType.DRAW:
            return Round.RoundResult.DRAW

    @staticmethod
    def __count_early_drops(events):
        """early drop - player disconnected withing first 30 secs
        of the round."""
        start_time = time(*[int(x) for x in events[0].time.split(":")])
        max_spawn_time = (datetime.combine(date.today(), start_time) +
                          timedelta(0, 30))

        num_of_early_drops = 0
        for event in events:
            if event.type in EventType.suicides() and time(*[int(x) for x in events[0].time.split(":")]) < max_spawn_time.time():
                num_of_early_drops += 1
        return num_of_early_drops

    @staticmethod
    def __evaluate_round(
            home_kills, away_kills, num_of_early_drops, flag, draw,
            required_num_of_kills, last_event_type):
        """Check if round meets all requirements to be considered valid match
        round."""
        kills_to_win = range(
            required_num_of_kills,
            required_num_of_kills+num_of_early_drops+1)

        if (len(home_kills) in kills_to_win or
            len(away_kills) in kills_to_win or
                flag or draw):
            if last_event_type in EventType.round_ending_with_selection():
                return RoundStatus.ACCEPTED_WITH_SELECTION
            return RoundStatus.ACCEPTED
        return RoundStatus.REJECTED
