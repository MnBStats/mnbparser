class Kill:
    def __init__(self, *, kill_number, killer, victim, kill_type, weapon_id,
                 teamkill):
        self.killer = killer
        self.victim = victim
        self.kill_type = kill_type
        self.weapon_id = weapon_id
        self.kill_number = kill_number
        self.teamkill = teamkill
        self.spawn_number = 0
        self.round_number = 0

    def __repr__(self):
        return "<Kill({})>".format(self.__dict__)

    def __str__(self):
        return "{} {} {}".format(self.killer, self.kill_type, self.victim)
