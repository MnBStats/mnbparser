from .utils import AutoNumberEnum


class EventType(AutoNumberEnum):
    """Enum representing event type"""
    MAP_CHANGE = ()
    MAP_START = ()
    ROUND_START = ()
    MAP_END = ()
    MAP_END_WITH_SELECTION = () # TODO: ITEM.99
    SPAWN_SELECTED = () # TODO: ITEM.99
    PLAYER_SPAWNED = ()
    HOME_TEAM_KILL = ()
    AWAY_TEAM_KILL = ()
    HOME_TEAM_TEAMKILL = ()
    AWAY_TEAM_TEAMKILL = ()
    HOME_TEAM_SUICIDE = ()
    AWAY_TEAM_SUICIDE = ()
    KILL_INFO = ()
    PLAYER_ASSIST = ()
    PLAYER_JOINED = ()
    TEAM_A_WIN = ()
    TEAM_A_WIN_FLAG = ()
    TEAM_B_WIN = ()
    TEAM_B_WIN_FLAG = ()
    DRAW = ()
    ERROR = ()

    @staticmethod
    def round_result_events():
        return {EventType.TEAM_A_WIN,
                EventType.TEAM_A_WIN_FLAG,
                EventType.TEAM_B_WIN,
                EventType.TEAM_B_WIN_FLAG,
                EventType.DRAW}

    @staticmethod
    def team_a_wins():
        return {EventType.TEAM_A_WIN,
                EventType.TEAM_A_WIN_FLAG}

    @staticmethod
    def team_b_wins():
        return {EventType.TEAM_B_WIN,
                EventType.TEAM_B_WIN_FLAG}

    @staticmethod
    def match_related_events():
        all_types = set(EventType)
        non_related = {EventType.ERROR,
                       EventType.PLAYER_JOINED}
        return all_types.difference(non_related)

    @staticmethod
    def round_starting_events():
        return {EventType.MAP_START,
                EventType.ROUND_START}

    @staticmethod
    def round_ending_events():
        return {EventType.MAP_END,
                EventType.MAP_END_WITH_SELECTION,
                EventType.SPAWN_SELECTED,
                EventType.ROUND_START}

    @staticmethod
    def round_ending_with_selection():
        return {EventType.MAP_END_WITH_SELECTION,
                EventType.SPAWN_SELECTED}

    @staticmethod
    def spawn_starting_events():
        return {EventType.MAP_START}

    @staticmethod
    def spawn_ending_events():
        return {EventType.MAP_END, EventType.MAP_END_WITH_SELECTION}

    @staticmethod
    def home_team_scores():
        return {EventType.HOME_TEAM_KILL,
                EventType.AWAY_TEAM_SUICIDE,
                EventType.AWAY_TEAM_TEAMKILL}

    @staticmethod
    def away_team_scores():
        return {EventType.AWAY_TEAM_KILL,
                EventType.HOME_TEAM_SUICIDE,
                EventType.HOME_TEAM_TEAMKILL}

    @staticmethod
    def teamkills():
        return {EventType.HOME_TEAM_TEAMKILL, EventType.AWAY_TEAM_TEAMKILL}

    @staticmethod
    def suicides():
        return {EventType.HOME_TEAM_SUICIDE, EventType.AWAY_TEAM_SUICIDE}

    @staticmethod
    def kill_types():
        return EventType.home_team_scores() | EventType.away_team_scores()


# TODO: ITEM.98: Consider inheritance for different Event types.
class Event:
    """Event is a result of parsing of a single line of log. It has required
    fields: type and time and few optional fields specific to Event type.
    """
    def __init__(self, event_type, time="", *, player="", victim="", kill_type="", weapon_id="", \
      dmg="", team_dmg="", assists="", side="", wb_class="", game_id="", map="", faction_one="", faction_two=""):
        self.type = event_type
        self.time = time
        self.player = player
        self.victim = victim
        self.kill_type = kill_type
        self.weapon_id = weapon_id
        self.dmg = dmg
        self.team_dmg = team_dmg
        self.assists = assists
        self.side = side
        self.wb_class = wb_class
        self.game_id = game_id
        self.map = map
        self.faction_one = faction_one
        self.faction_two = faction_two

    def __eq__(self, other):
        if not isinstance(other, Event):
            raise TypeError(other)
        return (self.type == other.type and
                self.time == other.time and
                self.player == other.player and
                self.victim == other.victim and
                self.kill_type == other.kill_type and
                self.weapon_id == other.weapon_id and
                self.dmg == other.dmg and
                self.team_dmg == other.team_dmg and
                self.assists == other.assists and
                self.side == other.side and
                self.wb_class == other.wb_class and
                self.game_id == other.game_id and
                self.map == other.map and
                self.faction_one == other.faction_one and
                self.faction_two == other.faction_two)

    def __repr__(self):
        return ("<Event (type: {0}, time: {1}, "
                "player: {2}, victim: {3}, game_id: {4}, kill_type: {5}, weapon_id: {6}, " \
            "dmg: {7}, team_dmg: {8}, assists: {9}, side: {10}, class: {11}, " \
            "map: {12}, faction_one: {13}, faction_two: {14})>") \
            .format(self.type.name, self.time,
                    self.player, self.victim, self.game_id, self.kill_type, self.weapon_id, self.dmg, self.team_dmg, self.assists, self.side, self.wb_class,
                    self.map, self.faction_one, self.faction_two)
