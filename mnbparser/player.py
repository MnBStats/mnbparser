class Player:
    def __init__(self, name, game_id):
        self.name = name
        self.game_id = game_id

    def __repr__(self):
        return ("<Player (name: {}, game_id: {}>"
                .format(self.name, self.game_id))

    def __str__(self):
        return "{}".format(self.name.full)

    def __eq__(self, other):
        if not isinstance(other, Player):
            raise TypeError(other)
        return self.name == other.name and self.game_id == other.game_id

    def __hash__(self):
        return hash(self.name) ^ hash(self.game_id)

    @classmethod
    def create(cls, event):
        return Player(name=event.player, game_id=event.game_id)


class Nickname:
    def __init__(self, name, tag):
        self.name = name
        self.tag = tag

    @property
    def full(self):
        return self.tag + self.name

    def __eq__(self, other):
        if not isinstance(other, Nickname):
            raise TypeError(other)
        return self.name == other.name and self.tag == other.tag

    def __repr__(self):
        return "<Nickname: (name: {0}, tag: {1})>".format(self.name, self.tag)

    def __str__(self):
        return "{0}{1}".format(self.tag, self.name)

    def __hash__(self):
        return hash(self.full)

    @classmethod
    def create(cls, word, tags):
        for tag in [tags.home, tags.away]:
            if word.startswith(tag):
                name = ''.join(word.split(tag, maxsplit=1))
                return Nickname(name, tag)
            if word.endswith(tag):
                name = ''.join(word.rsplit(tag, maxsplit=1))
                return Nickname(name, tag)

        return Nickname(word, "")
