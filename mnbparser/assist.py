class Assist:
    def __init__(self, *, player, dmg, team_dmg, number_of_assists):
        self.player = player
        self.dmg = dmg
        self.team_dmg = team_dmg
        self.number_of_assists = number_of_assists
        self.spawn_number = 0
        self.round_number = 0

    def __repr__(self):
        return "<Assist({})>".format(self.__dict__)

    def __str__(self):
        return "{} {} {} {}".format(
            self.player, self.dmg, self.team_dmg, self.number_of_assists)
