class Rules:
    def __init__(
            self, min_no_spawns, rounds_to_win, max_no_of_rounds, no_of_kills):
        self.min_number_of_spawns = min_no_spawns
        self.rounds_to_win = rounds_to_win
        self.max_number_of_rounds = max_no_of_rounds
        self.required_number_of_kills = no_of_kills

    @classmethod
    def create_default(cls):
        return Rules(
            min_no_spawns=4,
            rounds_to_win=3,
            max_no_of_rounds=5,
            no_of_kills=8)
