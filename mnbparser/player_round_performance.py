class SpawnedPlayer:
    def __init__(self, *, player, side, wb_class):
        self.player = player
        self.side = side
        self.wb_class = wb_class
        self.spawn_number = 0
        self.round_number = 0

    def __repr__(self):
        return "<SpawnedPlayer({})>".format(self.__dict__)

    def __str__(self):
        return "{} {} {}".format(self.player, self.side, self.wb_class)
