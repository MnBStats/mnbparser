from mnbparser.parser import Parser
from mnbparser.event import EventType
from .player import Player


def load_players(path, tags):
    """Loads players list from match log file"""
    parser = Parser.create_join_parser()
    return list(
        Player.create(event)
        for event in generate_events(parser, path, tags))


def load_match_related_events(path, teams):
    parser = Parser.create_match_parser()
    return list(generate_events(parser, path, teams))


def generate_events(parser, path, *args):
    with open(path, 'r', encoding='utf-8-sig') as file:
        for line in file:
            event = parser.parse(line, *args)
            if event.type != EventType.ERROR:
                yield event
