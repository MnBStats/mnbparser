class Score:
    def __init__(self, home_score, away_score):
        self.home_score = home_score
        self.away_score = away_score

    def __repr__(self):
        return '<Score({})'.format(self.__dict__)

    def __eq__(self, other):
        if not isinstance(other, Score):
            raise TypeError(other)
        return self.__dict__ == other.__dict__

    def __add__(self, other):
        _hs = self.home_score + other.home_score
        _as = self.away_score + other.away_score
        return Score(_hs, _as)

    def __radd__(self, other):
        if other == 0:
            return self
        else:
            return self.__add__(other)
