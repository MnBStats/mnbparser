from mnbparser.event import Event, EventType
from .parsing_options import PlayerSpawnHandler, KillHandler, AssistsHandler, \
    ServerEventHandler, JoinHandler, MapChangeHandler
from .log_lines import LogLine, LineTooShortError


class Parser:
    def __init__(self, handlers):
        self.handlers = handlers

    @classmethod
    def create_match_parser(cls):
        handlers = [
            PlayerSpawnHandler(), KillHandler(),
            AssistsHandler(), ServerEventHandler(), MapChangeHandler()]
        return cls(handlers)

    @classmethod
    def create_join_parser(cls):
        handlers = [JoinHandler()]
        return cls(handlers)

    def parse(self, line, teams, *args):
        try:
            log = LogLine(line)
        except LineTooShortError:
            return Event(EventType.ERROR)
        else:
            for handler in self.handlers:
                event = handler.parse(log, teams, *args)
                if event.type != EventType.ERROR:
                    return event

        return Event(EventType.ERROR)
