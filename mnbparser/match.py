from mnbparser.utils import AutoNumberEnum, print_numbered_range
from mnbparser.event_range import find_spawns_positions, find_map_info
from mnbparser.spawn import Spawn
from mnbparser.score import Score
from mnbparser.match_rules import Rules


class Match:
    class Status(AutoNumberEnum):
        SUCCESS = ()
        FAILURE = ()

    def __init__(self, teams, status, spawns, num_of_accepted_spawns, score):
        self.teams = teams
        self.status = status
        self.spawns = spawns
        self.num_of_accepted_spawns = num_of_accepted_spawns
        self.score = score

    def __repr__(self):
        return "<Match({})>".format(self.__dict__)

    def __str__(self):
        return ("Match {tags}\n"
                "Parsing status: {status}\n"
                "Number of accepted spawns: {num}\n"
                "Score: {score}\n"
                "Details:\n"
                "{spawns}").format(
                    tags=str(self.teams.tags),
                    status=self.status.name,
                    num=self.num_of_accepted_spawns,
                    spawns=print_numbered_range(self.spawns),
                    score=self.score)

    @property
    def kills(self):
        kills = list()
        for spawn_num, spawn in enumerate(self.spawns, start=1):
            for round_num, round in enumerate(spawn.rounds, start=1):
                for k in round.kills():
                    k.spawn_number = spawn_num
                    k.round_number = round_num
                    kills.append(k)

        return kills

    @property
    def assists(self):
        assists = list()
        for spawn_num, spawn in enumerate(self.spawns, start=1):
            for round_num, round in enumerate(spawn.rounds, start=1):
                for a in round.assists:
                    a.spawn_number = spawn_num
                    a.round_number = round_num
                    assists.append(a)

        return assists

    @property
    def spawned_players(self):
        spawned_players = list()
        for spawn_num, spawn in enumerate(self.spawns, start=1):
            for round_num, round in enumerate(spawn.rounds, start=1):
                for sp in round.spawned_players:
                    sp.spawn_number = spawn_num
                    sp.round_number = round_num
                    spawned_players.append(sp)

        return spawned_players

    @classmethod
    def create(cls, events, teams, rules=Rules.create_default()):
        spawns = list(cls.__generate_spawns(events, rules, teams))
        status, num_of_accepted_spawns = \
            cls.__evaluate_match(spawns, rules.min_number_of_spawns)

        spawns = trim_irrelevant_spawns(spawns, status)
        score = sum(s.score for s in spawns) \
            if status == Match.Status.SUCCESS else \
            Score(0, 0)

        return cls(
            teams=teams,
            status=status,
            spawns=spawns,
            num_of_accepted_spawns=num_of_accepted_spawns,
            score=score)

    @staticmethod
    def __generate_spawns(events, rules, teams):
        spawns_positions = find_spawns_positions(events)

        for pos in spawns_positions:
            map_info = find_map_info(events[:pos.start])
            spawn_events = events[pos.start:pos.end + 1]
            yield Spawn.create(events=spawn_events,
                map="unknown" if not map_info else map_info.map,
                attacking_faction="unknown" if not map_info else map_info.faction_one,
                defending_faction="unknown" if not map_info else map_info.faction_two, rules=rules, teams=teams)

    @staticmethod
    def __evaluate_match(spawns, min_number_of_spawns):
        num_of_accepted_spawns = \
            sum(s.status == Spawn.Status.ACCEPTED for s in spawns)

        if num_of_accepted_spawns == min_number_of_spawns:
            return Match.Status.SUCCESS, num_of_accepted_spawns

        return Match.Status.FAILURE, num_of_accepted_spawns


def trim_irrelevant_spawns(spawns, status):
    spawn_types = {Spawn.Status.ACCEPTED}

    if status == Match.Status.FAILURE:
        spawn_types.add(Spawn.Status.NO_SPAWN_CANDIDATE)

    return [s for s in spawns if s.status in spawn_types]
