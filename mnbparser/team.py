import itertools


class Tags:
    """Class for storing team tags."""

    def __init__(self, home, away):
        self.home = home
        self.away = away

    def __eq__(self, other):
        if not isinstance(other, Tags):
            raise TypeError(other)
        return self.home == other.home and self.away == other.away

    def __repr__(self):
        return "<Tags (home: {0}, away: {1}".format(self.home, self.away)

    def __str__(self):
        return "{} vs {}".format(self.home, self.away)

    def __contains__(self, item):
        return item in {self.home, self.away}


class Team:
    """Container class for players representing one team."""

    def __init__(self, players, tag):
        self.tag = tag
        self.players = {p.name: p for p in players}

    def __str__(self):
        return 'Team {}, players: {}'.format(self.tag, self.players)

    def __contains__(self, nickname):
        return nickname in self.players.keys()

    def __eq__(self, other):
        if not isinstance(other, Team):
            raise TypeError(other)
        return (self.players == other.players and
                self.tag == other.tag)

    def __iter__(self):
        return (p for p in self.players.values())

    def __getitem__(self, nickname):
        return self.players[nickname]


class Teams:
    """Class for storing players representing home and away team."""

    def __init__(self, home, away):
        self.home = home
        self.away = away
        self.tags = Tags(self.home.tag, self.away.tag)

    def __eq__(self, other):
        if not isinstance(other, Teams):
            raise TypeError(other)
        return self.home == other.home and self.away == other.away

    def __repr__(self):
        return ("<Identifiers: (home: {0}, away: {1})>"
                .format(self.home, self.away))

    def __contains__(self, nickname):
        return (nickname in self.home or
                nickname in self.away)

    def __iter__(self):
        return (p for p in itertools.chain(self.home, self.away))

    def get_player(self, nickname):
        if nickname in self.home:
            return self.home[nickname]
        if nickname in self.away:
            return self.away[nickname]

        return None
