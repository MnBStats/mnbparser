import unittest
from mnbparser.parser import Parser
from mnbparser.event import Event, EventType
from mnbparser.player import Nickname, Player
from mnbparser.team import Teams, Team, Tags

HOME_TAG = "IR_"
AWAY_TAG = "Ex_"
HOME_NAME = "Iovca"
AWAY_NAME = "Novac"
HOME_PLAYER = Player(name=Nickname(HOME_NAME, HOME_TAG), game_id=1)
AWAY_PLAYER = Player(name=Nickname(AWAY_NAME, AWAY_TAG), game_id=2)
KILL_TYPE = "<img=ico_headshot>"
TAGS = Tags(HOME_TAG, AWAY_TAG)
HOME_TEAM = Team({HOME_PLAYER}, HOME_TAG)
AWAY_TEAM = Team({AWAY_PLAYER}, AWAY_TAG)
TEAMS = Teams(HOME_TEAM, AWAY_TEAM)
TIME = "20:00:00"
GAME_ID = "1136199"

MAP_CHANGE_LINE = "{0} - Changed to Battle on Vendetta, Kingdom of Swadia vs Kingdom of Nords by IR_Iovca.".format(TIME)
MAP_START_LINE = "{0} - [EVENT]: Map start".format(TIME)
ROUND_START_LINE = "{0} - [EVENT]: Round start. Score at 0 - 0 (0 draws)".format(TIME)
ROUND_START_TEAM_A_WIN_LINE = "{0} - [EVENT]: Round start. Score at 3 - 0 (0 draws)".format(TIME)
ROUND_START_TEAM_B_WIN_LINE = "{0} - [EVENT]: Round start. Score at 0 - 3 (0 draws)".format(TIME)
PLAYER_SPAWN_LINE = "{0} - Player: IR_Iovca, Team: 1 (Sarranid Sultanate), Class: Sarranid Mamluke".format(TIME)
TEAM_A_KILL_LINE = "{0} - IR_Iovca {kill_type} Ex_Novac".format(TIME, kill_type=KILL_TYPE)
TEAM_B_KILL_LINE = "{0} - Ex_Novac {kill_type} IR_Iovca".format(TIME, kill_type=KILL_TYPE)
TEAM_A_TEAMKILL_LINE = "{0} - IR_Iovca {kill_type} IR_Iovca".format(TIME, kill_type=KILL_TYPE)
TEAM_B_TEAMKILL_LINE = "{0} - Ex_Novac {kill_type} Ex_Novac".format(TIME, kill_type=KILL_TYPE)
TEAM_A_TEAMKILL_END_TAG_LINE = "{0} - IovcaIR_ {kill_type} IovcaIR_".format(TIME, kill_type=KILL_TYPE)
TEAM_B_TEAMKILL_END_TAG_LINE = "{0} - NovacEx_ {kill_type} NovacEx_".format(TIME, kill_type=KILL_TYPE)
TEAM_A_SUICIDE_LINE = "{0} -  <img=ico_headshot> IR_Iovca".format(TIME)
TEAM_B_SUICIDE_LINE = "{0} -  <img=ico_headshot> Ex_Novac".format(TIME)
KILL_INFO_LINE_ONE_WORD_WEAPON = "{0} - IR_Iovca <weapon=Sword> Ex_Novac".format(TIME)
KILL_INFO_LINE_MULTIPLE_WORDS_WEAPON = "{0} - IR_Iovca <weapon=Great Long Axe> Ex_Novac".format(TIME)
FLAG_RAISED_LINE = "{0} - [EVENT]: Round result: Team 1 win by flag higher.".format(TIME)
PLAYER_DAMAGE_AND_ASSISTS_LINE = "{0} - IR_Iovca has done 22 damage this round, and 0 teamdamage this round, and has 1 assist".format(TIME)
MAP_END_LINE = "{0} - [EVENT]: Map ended. Score is 0 - 0 (0 draws)".format(TIME)
MAP_END_TEAM_A_WINS_LINE = "{0} - [EVENT]: Map ended. Score is 3 - 0 (0 draws)".format(TIME)
MAP_END_TEAM_B_WINS_LINE = "{0} - [EVENT]: Map ended. Score is 0 - 3 (0 draws)".format(TIME)
TEAM_A_WIN_LINE = '{0} - [EVENT]: Round result: Team 1 win.'.format(TIME)
TEAM_A_WIN_FLAG_LINE = "{0} - [EVENT]: Round result: Team 1 win by flag higher.".format(TIME)
TEAM_B_WIN_LINE = '{0} - [EVENT]: Round result: Team 2 win.'.format(TIME)
TEAM_B_WIN_FLAG_LINE = "{0} - [EVENT]: Round result: Team 2 win by flag higher.".format(TIME)
DRAW_LINE = "{0} - [EVENT]: Round result draw".format(TIME)
WRONG_TEAM_PLAYER_JOINED_LINE = "{0} - DUPA_Harman has joined the game with ID: 5".format(TIME)
PLAYER_JOINED_LINE = "{0} - Ex_Novac has joined the game with ID: 1136199 and has administrator rights.".format(TIME)
GARBAGE_LINE = "Garbage choko loko boko"


class MatchParserTestSuite(unittest.TestCase):
    def setUp(self):
        self.parser = Parser.create_match_parser()

    def parse(self, line):
        return self.parser.parse(line, TEAMS)

    def test_player_joined(self):
        self.assertEqual(Event(EventType.ERROR), self.parse(PLAYER_JOINED_LINE))

    def test_map_change(self):
        self.assertEqual(Event(EventType.ERROR), self.parse(MAP_CHANGE_LINE))

    def test_error(self):
        self.assertEqual(Event(EventType.ERROR), self.parse(""))
        self.assertEqual(Event(EventType.ERROR), self.parse(GARBAGE_LINE))

    def test_map_start(self):
        self.assertEqual(Event(EventType.MAP_START, TIME),
                         self.parse(MAP_START_LINE))

    def test_round_start(self):
        self.assertEqual(Event(EventType.ROUND_START, TIME),
                         self.parse(ROUND_START_LINE))

    def test_round_end_with_selection(self):
        self.assertEqual(Event(EventType.SPAWN_SELECTED, TIME),
                         self.parse(ROUND_START_TEAM_A_WIN_LINE))

        self.assertEqual(Event(EventType.SPAWN_SELECTED, TIME),
                         self.parse(ROUND_START_TEAM_B_WIN_LINE))

    def test_map_end(self):
        self.assertEqual(Event(EventType.MAP_END, TIME),
                         self.parse(MAP_END_LINE))

    def test_map_end_with_selection(self):
        self.assertEqual(Event(EventType.MAP_END_WITH_SELECTION, TIME),
                         self.parse(MAP_END_TEAM_A_WINS_LINE))

        self.assertEqual(Event(EventType.MAP_END_WITH_SELECTION, TIME),
                         self.parse(MAP_END_TEAM_B_WINS_LINE))

    def test_draw(self):
        self.assertEqual(
            Event(EventType.TEAM_A_WIN, TIME), self.parse(TEAM_A_WIN_LINE))
        self.assertEqual(
            Event(EventType.TEAM_A_WIN_FLAG, TIME), self.parse(TEAM_A_WIN_FLAG_LINE))
        self.assertEqual(
            Event(EventType.TEAM_B_WIN, TIME), self.parse(TEAM_B_WIN_LINE))
        self.assertEqual(
            Event(EventType.TEAM_B_WIN_FLAG, TIME), self.parse(TEAM_B_WIN_FLAG_LINE))
        self.assertEqual(
            Event(EventType.DRAW, TIME), self.parse(DRAW_LINE))

    def test_team_a_kill(self):
        event = Event(
            EventType.HOME_TEAM_KILL, TIME,
            player=Nickname(HOME_NAME, HOME_TAG),
            victim=Nickname(AWAY_NAME, AWAY_TAG),
            kill_type=KILL_TYPE)

        self.assertEqual(event, self.parse(TEAM_A_KILL_LINE))

    def test_team_b_kill(self):
        event = Event(
            EventType.AWAY_TEAM_KILL, TIME,
            player=Nickname(AWAY_NAME, AWAY_TAG),
            victim=Nickname(HOME_NAME, HOME_TAG),
            kill_type=KILL_TYPE)

        self.assertEqual(event, self.parse(TEAM_B_KILL_LINE))

    def test_team_a_teamkill(self):
        event = Event(
            EventType.HOME_TEAM_TEAMKILL, TIME,
            player=Nickname(HOME_NAME, HOME_TAG),
            victim=Nickname(HOME_NAME, HOME_TAG),
            kill_type=KILL_TYPE)

        self.assertEqual(event, self.parse(TEAM_A_TEAMKILL_LINE))

    def test_team_b_teamkill(self):
        event = Event(
            EventType.AWAY_TEAM_TEAMKILL, TIME,
            player=Nickname(AWAY_NAME, AWAY_TAG),
            victim=Nickname(AWAY_NAME, AWAY_TAG),
            kill_type=KILL_TYPE)

        self.assertEqual(event, self.parse(TEAM_B_TEAMKILL_LINE))

    def test_team_a_teamkill_tags_at_the_end(self):
        event = Event(
            EventType.HOME_TEAM_TEAMKILL, TIME,
            player=Nickname(HOME_NAME, HOME_TAG),
            victim=Nickname(HOME_NAME, HOME_TAG),
            kill_type=KILL_TYPE)

        self.assertEqual(event, self.parse(TEAM_A_TEAMKILL_END_TAG_LINE))

    def test_team_b_teamkill_tags_at_the_end(self):
        event = Event(
            EventType.AWAY_TEAM_TEAMKILL, TIME,
            player=Nickname(AWAY_NAME, AWAY_TAG),
            victim=Nickname(AWAY_NAME, AWAY_TAG),
            kill_type=KILL_TYPE)

        self.assertEqual(event, self.parse(TEAM_B_TEAMKILL_END_TAG_LINE))

    def test_team_a_suicide(self):
        event = Event(
            EventType.HOME_TEAM_SUICIDE, TIME,
            player=Nickname("", ""),
            victim=Nickname(HOME_NAME, HOME_TAG),
            kill_type=KILL_TYPE)

        self.assertEqual(event, self.parse(TEAM_A_SUICIDE_LINE))

    def test_team_b_suicide(self):
        event = Event(
            EventType.AWAY_TEAM_SUICIDE, TIME,
            player=Nickname("", ""),
            victim=Nickname(AWAY_NAME, AWAY_TAG),
            kill_type=KILL_TYPE)

        self.assertEqual(event, self.parse(TEAM_B_SUICIDE_LINE))

    def test_player_damage_and_assists_line(self):
        event = Event(
            EventType.PLAYER_ASSIST, TIME,
            player=Nickname(HOME_NAME, HOME_TAG),
            dmg="22",
            team_dmg="0",
            assists="1")

        self.assertEqual(event, self.parse(PLAYER_DAMAGE_AND_ASSISTS_LINE))

    def test_player_spawn_line(self):
        event = Event(
            EventType.PLAYER_SPAWNED, TIME,
            player=Nickname(HOME_NAME, HOME_TAG),
            side="1",
            wb_class="Mamluke")

        self.assertEqual(event, self.parse(PLAYER_SPAWN_LINE))

    def test_kill_info_line_one_word(self):
        event = Event(
            EventType.KILL_INFO, TIME,
            player=Nickname(HOME_NAME, HOME_TAG),
            victim=Nickname(AWAY_NAME, AWAY_TAG),
            weapon_id="Sword")

        self.assertEqual(event, self.parse(KILL_INFO_LINE_ONE_WORD_WEAPON))

    def test_kill_info_line_multiple_words(self):
        event = Event(
            EventType.KILL_INFO, TIME,
            player=Nickname(HOME_NAME, HOME_TAG),
            victim=Nickname(AWAY_NAME, AWAY_TAG),
            weapon_id="Great Long Axe")

        self.assertEqual(event, self.parse(KILL_INFO_LINE_MULTIPLE_WORDS_WEAPON))

    def test_map_change_line(self):
        event = Event(
            EventType.MAP_CHANGE, TIME,
            map="Vendetta",
            faction_one="Kingdom of Swadia",
            faction_two="Kingdom of Nords")

        self.assertEqual(event, self.parse(MAP_CHANGE_LINE))


# TODO: ITEM.96: Add tests for Join parser test suite
class JoinParserTestSuite(unittest.TestCase):
    def setUp(self):
        self.parser = Parser.create_join_parser()

    def test_player_joined(self):
        expected = Event(
            EventType.PLAYER_JOINED,
            TIME,
            player=Nickname(AWAY_NAME, AWAY_TAG),
            game_id=GAME_ID)

        self.assertEqual(expected, self.parser.parse(PLAYER_JOINED_LINE, TAGS))


if __name__ == '__main__':
    unittest.main()
