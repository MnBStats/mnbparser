import unittest
from mnbparser import match_rules
from mnbparser.match import Match
from mnbparser.team import Teams, Team
from mnbparser.player import Nickname, Player
from mnbparser.event_loader import load_match_related_events
from mnbparser.event import EventType
from mnbparser.score import Score


class MatchEvaluationTestSuite(unittest.TestCase):
    def evaluate_match(self, path):
        home_tag = "FIN_"
        away_tag = "Ireland_"

        home_team = Team(
            players={
                Player(Nickname(name="axL", tag=home_tag), 1),
                Player(Nickname(name="Meldo", tag=home_tag), 2),
                Player(Nickname(name="Varangian", tag=home_tag), 3),
                Player(Nickname(name="Dopey", tag=home_tag), 4),
                Player(Nickname(name="Mr_Boston", tag=home_tag), 5),
                Player(Nickname(name="SotaMursu", tag=home_tag), 6),
                Player(Nickname(name="mOnkey", tag=home_tag), 7),
                Player(Nickname(name="Bauglir", tag=home_tag), 8),
                Player(Nickname(name="Raven", tag=home_tag), 9)},
            tag=home_tag)

        away_team = Team(
            players={
                Player(Nickname(name="Macco", tag=away_tag), 10),
                Player(Nickname(name="Kiano", tag=away_tag), 11),
                Player(Nickname(name="Flans", tag=away_tag), 12),
                Player(Nickname(name="Goldenblack", tag=away_tag), 13),
                Player(Nickname(name="Seman", tag=away_tag), 14),
                Player(Nickname(name="Pestyknight", tag=away_tag), 15)},
            tag=away_tag)

        teams = Teams(home_team, away_team)
        events = load_match_related_events(path, teams)

        rules = match_rules.Rules(
            min_no_spawns=1,
            rounds_to_win=3,
            max_no_of_rounds=5,
            no_of_kills=6)

        match = Match.create(events, teams, rules)

        self.assertEqual(Match.Status.SUCCESS, match.status)
        self.assertEqual(1, len(match.spawns))
        self.assertEqual(1, match.num_of_accepted_spawns)
        self.assertEqual(3, len(match.spawns[0].rounds))
        self.assertEqual(6, sum(e.type == EventType.HOME_TEAM_KILL
                                for e in match.spawns[0].rounds[0].events))
        self.assertEqual(22, len(match.kills))
        return match.score

    def test_ecs_admin_mod_match_evaluation(self):
        score = self.evaluate_match(path="FIN_VSIreland_ECSAdminMod")
        self.assertEqual(Score(home_score=0, away_score=0), score)

    def test_nc17_admin_mod_match_evaluation(self):
        score = self.evaluate_match(path="FIN_VSIreland_NC17AdminMod")
        self.assertEqual(Score(home_score=0, away_score=3), score)


if __name__ == '__main__':
    unittest.main()
