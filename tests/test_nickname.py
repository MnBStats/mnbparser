import unittest
from mnbparser.player import Nickname
from mnbparser.team import Tags

NAME = "Name"
TAG_A = "A_"
TAG_B = "B_"
TAGS = Tags(TAG_A, TAG_B)


class NicknameTestSuite(unittest.TestCase):
    def test_empty_word_empty_result(self):
        result = Nickname(name="", tag="")
        self.assertEqual(result, Nickname.create("", TAGS))

    def test_word_without_tag(self):
        result = Nickname(name=NAME, tag="")
        self.assertEqual(result, Nickname.create(NAME, TAGS))

    def test_word_starts_with_tag_a(self):
        word = TAG_A + NAME
        result = Nickname(name=NAME, tag=TAG_A)
        self.assertEqual(result, Nickname.create(word, TAGS))

    def test_word_ends_with_tag_a(self):
        word = NAME + TAG_A
        result = Nickname(name=NAME, tag=TAG_A)
        self.assertEqual(result, Nickname.create(word, TAGS))

    def test_word_with_additional_tag_a_in_the_middle(self):
        word = TAG_A + TAG_A + NAME
        result = Nickname(name=TAG_A + NAME, tag=TAG_A)
        self.assertEqual(result, Nickname.create(word, TAGS))

    def test_word_starts_with_tag_b(self):
        word = TAG_B + NAME
        result = Nickname(name=NAME, tag=TAG_B)
        self.assertEqual(result, Nickname.create(word, TAGS))

    def test_word_ends_with_tag_b(self):
        word = NAME + TAG_B
        result = Nickname(name=NAME, tag=TAG_B)
        self.assertEqual(result, Nickname.create(word, TAGS))

    def test_word_ends_with_tag_b_x2(self):
        word = NAME + TAG_B + TAG_B
        result = Nickname(name=NAME + TAG_B, tag=TAG_B)
        self.assertEqual(result, Nickname.create(word, TAGS))


if __name__ == '__main__':
    unittest.main()
