import unittest
from mnbparser.event import Event, EventType
from mnbparser import event_range as ER

EventRange = ER.EventRange
find_spawns_positions = ER.find_spawns_positions
find_rounds_positions = ER.find_rounds_positions


TIME = "20:00:00"
MAP_START = Event(EventType.MAP_START, TIME)
ROUND_START = Event(EventType.ROUND_START, TIME)
MAP_END = Event(EventType.MAP_END, TIME)
MAP_END_WITH_SELECTION = Event(EventType.MAP_END_WITH_SELECTION, TIME)
SPAWN_SELECTED = Event(EventType.SPAWN_SELECTED, TIME)
DIFFERENT_EVENT = Event(EventType.HOME_TEAM_KILL, TIME)
START_EVENT_TYPES = {EventType.ROUND_START}
END_EVENT_TYPES = {EventType.MAP_END}


class FindSpawnsPositionsTestSuite(unittest.TestCase):
    def test_0_events_0_spawns(self):
        self.assertEqual([], find_spawns_positions([]))

    def test_no_map_start_no_spawns(self):
        events = [DIFFERENT_EVENT]
        self.assertEqual([], find_spawns_positions(events))

    def test_spawn_is_range_between_map_start_and_map_end(self):
        events = [MAP_START, MAP_END_WITH_SELECTION]
        self.assertEqual([EventRange(0, 1)], find_spawns_positions(events))

    def test_map_end_with_selection_also_counts_as_map_end(self):
        events = [MAP_START, MAP_END_WITH_SELECTION]
        self.assertEqual([EventRange(0, 1)], find_spawns_positions(events))

    def test_last_map_start_can_exist_without_map_end_due_to_server_restart(self):
        """In this case select last range without ending event."""
        events = [MAP_START, MAP_START, DIFFERENT_EVENT, DIFFERENT_EVENT]
        self.assertEqual([EventRange(1, 3)], find_spawns_positions(events))

    def test_BUG_two_rapid_map_end_due_to_double_map_change(self):
        """
        BUG: Two players restarted the map at the same time after map end.
        Only one range should be selected in this case.
        """
        events = [MAP_START, MAP_END_WITH_SELECTION, MAP_END_WITH_SELECTION]
        self.assertEqual([EventRange(0, 1)], find_spawns_positions(events))


class FindRoundsPositionsTestSuite(unittest.TestCase):
    def test_0_events_0_rounds(self):
        self.assertEqual([], find_rounds_positions([]))

    def test_no_round_start_no_rounds(self):
        events = [DIFFERENT_EVENT]
        self.assertEqual([], find_rounds_positions(events))

    def test_map_start_map_end(self):
        events = [MAP_START, DIFFERENT_EVENT, MAP_END]
        self.assertEqual([EventRange(0, 2)], find_rounds_positions(events))

    def test_map_start_map_end_with_selection(self):
        events = [MAP_START, DIFFERENT_EVENT, MAP_END_WITH_SELECTION]
        self.assertEqual([EventRange(0, 2)], find_rounds_positions(events))

    def test_last_map_start_can_exist_without_map_end_due_to_server_restart(self):
        """In this case select last range without ending event."""
        events = [MAP_START]
        self.assertEqual([EventRange(0, 0)], find_rounds_positions(events))

    def test_last_round_start_can_exist_without_ending_event_due_to_server_restart(self):
        """In this case select last range without ending event."""
        events = [MAP_START, ROUND_START, DIFFERENT_EVENT]
        rounds_positions = [EventRange(0, 1), EventRange(1, 2)]
        self.assertEqual(rounds_positions, find_rounds_positions(events))

    def test_usual_successful_scenario(self):
        events = [
            MAP_START, DIFFERENT_EVENT,
            ROUND_START, DIFFERENT_EVENT,
            ROUND_START, DIFFERENT_EVENT,
            SPAWN_SELECTED, MAP_END]

        spawns_positions = [
            ER.EventRange(0, 2), ER.EventRange(2, 4), ER.EventRange(4, 6)]
        self.assertEqual(spawns_positions, find_rounds_positions(events))

    def test_BUG_two_rapid_map_end_due_to_double_map_change(self):
        """
        BUG: Two players restarted the map at the same time after map end.
        Only one range should be selected in this case.
        """
        events = [MAP_START, MAP_END_WITH_SELECTION, MAP_END_WITH_SELECTION]
        self.assertEqual([ER.EventRange(0, 1)], ER.find_rounds_positions(events))


@ER.first_event_in(START_EVENT_TYPES)
def decorated_function(events):
    pass


class ValidateEventsTestSuite(unittest.TestCase):
    def test_raise_when_no_events(self):
        events = []
        with self.assertRaises(ER.EventRangeInvalidInput) as context:
            decorated_function(events=events)
        msg = "No events on the list. Cannot process range."
        self.assertTrue(msg in str(context.exception))

    def test_raise_when_no_round_starting_event_in_front(self):
        events = [MAP_END]
        with self.assertRaises(ER.EventRangeInvalidInput) as context:
            decorated_function(events=events)
        msg = ("Events do not start with correct event. "
               "Cannot process range.")
        self.assertEqual(msg, str(context.exception))


if __name__ == '__main__':
    unittest.main()
